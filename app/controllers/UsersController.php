<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$validations = User::validate(Input::all());
		if($validations->passes()){
			$user = User::create(array(
				'username'     => Input::get('username'),
				'brandname'    => Input::get('brandname'),
				'email'        => Input::get('email'),
				'password'     => Hash::make(Input::get('password'))
			));

			// Save the security token for the activation email
			$user->token = uniqid();
			$user->expire_token = date('Y-m-d H:i:s');
			$user->save();

			return Redirect::route('users.create')->with('success','An email has been sent to your email box to activate your account');
		}else{
			Session::flash('error','Please check your errors');
			return Redirect::route('users.create')->withInput()->withErrors($validations);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		//
	}
}