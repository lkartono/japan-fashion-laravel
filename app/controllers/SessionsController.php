<?php

class SessionsController extends \BaseController {


	/**
	 * Show the public signin form
	 * Only display the view. Call from a GET request
	 * @return Response
	 */
	public function create() {
		return View::make('sessions.create');
	}



	/**
	 * Create a new user sessions.
	 * Will basically authenticate user. POST request
	 * @return Response
	 */
	public function store() {
		if(Input::has('username') && Input::has('password')){
			$user = array('username' => Input::get('username'), 'password' => Input::get('password'), 'active' => 1);
			if(Auth::attempt($user)){
				return Redirect::route('root_path')->with('success','Welcome to your account');
			}else{
				return Redirect::route('sessions.create')->with('error','Invalid username and password');
			}
		}
	}



	/**
	 * Delete a user session
	 * Simply logout a user from the system. DELETE request
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy() {
		Auth::logout();
		return Redirect::route('sessions.create');
	}
}