@extends('layouts.master')

@section('content')
    Register page
    {{ Form::open(array('route' => 'users.store')) }}
        {{ Form::label('username', 'Username') }}
        {{ Form::text('username', Input::old('username')) }}
        {{ $errors->first('username') }}
        <br>
        {{ Form::label('brandname', 'Brandname') }}
        {{ Form::text('brandname', Input::old('brandname')) }}
        {{ $errors->first('brandname') }}
        <br>
        {{ Form::label('email', 'Email address') }}
        {{ Form::text('email', Input::old('email')) }}
        {{ $errors->first('email') }}
        <br>
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password') }}
        {{ $errors->first('password') }}
        <br>
        {{ Form::label('password_confirmation', 'Confirm Password') }}
        {{ Form::password('password_confirmation') }}
        {{ $errors->first('password_confirmation') }}
        <br>
        {{ Form::submit('Register') }}
    {{ Form::close() }}
@stop