@if(Session::has('error'))
    <div class="error">
        {{ Session::get('error') }}
    </div>
@endif

@if(Session::has('success'))
    <div class="success">
        {{ Session::get('success') }}
    </div>
@endif