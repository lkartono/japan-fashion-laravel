@extends('layouts.master')

@section('content')
    Signin page
    {{ Form::open(array('route' => 'sessions.store')) }}
        {{ Form::label('username', 'Username') }}
        {{ Form::text('username') }}

        {{ Form::label('password', 'Password') }}
        {{ Form::password('password') }}

        {{ Form::submit('Connect') }}
    {{ Form::close() }}
@stop