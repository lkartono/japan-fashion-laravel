<?php

Route::get('/', array('as' => 'root_path', 'uses' => 'PagesController@index'));


// Routes related to the sessions controller (sigin, logout)
Route::resource('sessions', 'SessionsController');


// Route related to the users managment
Route::resource('users', 'UsersController');