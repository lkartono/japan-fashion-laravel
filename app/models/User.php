<?php

use Illuminate\Auth\UserInterface;

class User extends ActiveRecord implements UserInterface {


	protected $fillable = array('username', 'brandname', 'email', 'password', 'slug');
	protected $hidden   = array('password');


	public static $rules = array(
		'username'              => 'required|unique:users|min:5|alpha_num',
		'brandname'             => 'required|unique:users|min:5',
		'email'                 => 'required|unique:users|email',
		'password'              => 'required|between:5,15|confirmed',
		'password_confirmation' => 'required|between:5,15'
	);


	public static $sluggable = array(
		'build_from' => 'brandname'
	);


	/**
	 * Get the unique identifier for the user.
	 * @return mixed
	 */
	public function getAuthIdentifier() {
		return $this->getKey();
	}


	/**
	 * Get the password for the user.
	 * @return string
	 */
	public function getAuthPassword() {
		return $this->password;
	}
}