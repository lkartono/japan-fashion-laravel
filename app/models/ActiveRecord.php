<?php

class ActiveRecord extends Eloquent {

    /**
     * Common method shortcut allowing us to 
     * validate the input for a model
     * @param array $data Input post on form
     * @return boolean True if valid
     */
    public static function validate( $data ){
        return Validator::make($data, static::$rules);
    }
}