<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('lastname');
			$table->string('firstname');
			$table->string('brandname');
			$table->string('slug');
			$table->string('username');
			$table->string('password');
			$table->string('email');
			$table->string('logo');
			$table->string('banner');
			$table->text('about');
			$table->string('website');
			$table->string('tweeter_link');
			$table->string('facebook_link');
			$table->integer('fbid');
			$table->boolean('active')->default(0);
			$table->string('token');
			$table->datetime('expire_token');
			$table->string('role')->default('user');
			$table->datetime('last_login');
			$table->datetime('subscription');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
